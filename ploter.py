import matplotlib.pyplot as plt
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

import updater # con esto actualizamos los datos

CSV_ALTAS      = 'datasets/altas.csv'
CSV_CASOS      = 'datasets/casos.csv'
CSV_FALLECIDOS = 'datasets/fallecidos.csv'

df_altas = pd.read_csv(CSV_ALTAS, delimiter=',', index_col='cod_ine')
df_casos = pd.read_csv(CSV_CASOS, delimiter=',', index_col='cod_ine')
df_fallecidos = pd.read_csv(CSV_FALLECIDOS, delimiter=',', index_col='cod_ine')

# print(df_casos)

fig, axs = plt.subplots(3, 1, figsize=(12, 24), sharex=True)
axs = axs.flatten()

def ploteo_ind(df, list_selected_ccaa, ax, titulo, acumulado = False):
    list_ccaa = df.pop('CCAA').tolist()
    X = list(df.columns)
    X = pd.to_datetime(X, format='%Y/%m/%d')
    Y = df.to_numpy()
    for (ccaa, data) in zip(list_ccaa, Y):
        if ccaa in list_selected_ccaa:
            if acumulado or len(list_selected_ccaa) > 1:
                ax.plot(X, data, label = ccaa)
            else:
                data2 = []
                for i, e in enumerate(data):
                    if i == 0:
                        data2.append(e)
                    else:
                        data2.append(e - data[i-1])
                ax.bar(X, data2, label = ccaa)
                print(data2)
    ax.legend()
    ax.set_title(titulo)

# ploteo_ind(df_casos, ['Cantabria', 'Asturias'], axs[0], 'Casos')
# ploteo_ind(df_altas, ['Cantabria', 'Asturias'], axs[1], 'Altas')
# ploteo_ind(df_fallecidos, ['Cantabria', 'Asturias'], axs[2], 'Fallecidos')

ploteo_ind(df_casos, ['Asturias'], axs[0], 'Casos')
ploteo_ind(df_altas, ['Asturias'], axs[1], 'Altas')
ploteo_ind(df_fallecidos, ['Asturias'], axs[2], 'Fallecidos')


plt.savefig("data.png")
plt.show()
