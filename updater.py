import subprocess

URL_ALTAS      = "https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_altas.csv"
URL_CASOS      = "https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_casos.csv"
URL_FALLECIDOS = "https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_fallecidos.csv"
CSV_ALTAS      = 'datasets/altas.csv'
CSV_CASOS      = 'datasets/casos.csv'
CSV_FALLECIDOS = 'datasets/fallecidos.csv'


subprocess.call('curl -s {} > {}'.format(URL_ALTAS, CSV_ALTAS), shell=True)
subprocess.call('curl -s {} > {}'.format(URL_CASOS, CSV_CASOS), shell=True)
subprocess.call('curl -s {} > {}'.format(URL_FALLECIDOS, CSV_FALLECIDOS), shell=True)




